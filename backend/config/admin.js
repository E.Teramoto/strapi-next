module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '1b22a673bd4512f3c6bc982636867208'),
  },
});
